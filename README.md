# JS Utils

## Purpose
This in intended to be a shared project used by multiple UFIS Laravel projects, providing common functionality and shared utilities. This project should only contain shared front-end resources, any PHP/Laravel utilities should be added to the PHP Utilities project.

## Versioning
Composer has basic support for git repositories, but it does not support full semantic versioning syntax. So, it falls on us to manage the versioning manually. Whenever a new version is merged into the master branch that we want to be pulled into our projects, we should create a new tag following semantic versioning rules.