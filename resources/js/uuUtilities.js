let exports = module.exports = {};

try {
    global.$ = global.jQuery = require('jquery');
    window._ = require('lodash');
    window.moment = require('moment');
} catch (e) {
}


exports.inBrowser = typeof window !== 'undefined';
exports.UA = exports.inBrowser && window.navigator.userAgent.toLowerCase();
exports.isIE = exports.UA && /msie|trident/.test(exports.UA);

exports.getUrlParameter = function(sParam) {
    let sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

exports.setOnSessionStorage = function(index, elem) {
    if(typeof Storage === 'undefined') {
        return;
    }
    if(typeof elem === 'object') {
        elem = JSON.stringify(elem);
    }
    sessionStorage[index] = elem;
};

exports.getFromSessionStorage = function(index) {
    if(typeof Storage === 'undefined') {
        return;
    }
    try {
        return JSON.parse(sessionStorage[index]);
    } catch(e) {
        // If this is a failure, then it's not JSON
    }
    return sessionStorage[index];
};

exports.getFlashMessages = function() {
    let messages = exports.getFromSessionStorage('flash_messages');
    exports.setOnSessionStorage('flash_messages', '');
    return messages;
};

exports.setFlashMessages = function(msgArray) {
    let flashMsgArray = [];
    if(exports.getFromSessionStorage('flash_messages')) {
        flashMsgArray = exports.getFromSessionStorage('flash_messages');
    }
    $.each(msgArray, function(index, msg) {
        flashMsgArray.push(msg);
    });
    exports.setOnSessionStorage('flash_messages', flashMsgArray);
};

// This should be converted to a component
exports.initializeFlash = function() {
    let flashMsg = $('#flash-msg');
    if(!flashMsg.length) {
        flashMsg = $('<div>').attr('id', 'flash-msg').attr('class', 'alert alert-success alert-dismissible fade in').prependTo('#app-content');
        flashMsg.append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    }
    let msgArray = exports.getFlashMessages();
    if(msgArray) {
        $.each(msgArray, function(index, msg) {
            flashMsg.append($('<div>').text(msg));
        });
        flashMsg.show();
    }
};

// Credit to https://davidwalsh.name/nested-objects
exports.Objectifier = (function() {

    // Utility method to get and set objects that may or may not exist
    var objectifier = function(splits, create, context) {
        var result = context || window;
        for(var i = 0, s; result && (s = splits[i]); i++) {
            result = (s in result ? result[s] : (create ? result[s] = {} : undefined));
        }
        return result;
    };

    return {
        // Creates an object if it doesn't already exist
        set: function(name, value, context) {
            var splits = name.split('.'), s = splits.pop(), result = objectifier(splits, true, context);
            // This may come in handy if we need to force reactivity on every SET - so far, it hasn't been needed
            // if(result && s) {
            //     try { Vue.set(result, s, value); } catch(e) {}
            //     return result[s] = value;
            // } else {
            //     return undefined;
            // }
            return result && s ? (result[s] = value) : undefined;
        },
        get: function(name, create, context) {
            return objectifier(name.split('.'), create, context);
        },
        exists: function(name, context) {
            return this.get(name, false, context) !== undefined;
        }
    };

})();

